
$fn = 100;

arm();
runTests();
pusher();

// CONSTANTS
g = 32*12; // gravitational acceleration (32 ft/s^2)(12 in/ft)
blockWeight = 113; //nominal weight of block with three weights, verified by mesurement (g)
ballWeight = 30.1; //nominal weight of ball, verified by mesurment (g)

// PHYSICAL TEST PARAMETERS FOR THE ROBOT
vmin = 11; // vertical distance (height) of the shoulder axis from the wrist joint when reaching forward to the floor level (in)
hmin = 11; // horizontal distance from the shoulder axis to the wrist joint when reaching to the floor level (in)
lowLevelHeight = 3.0; // +/-1.5 for tipping
midLevelHeight = 8.5; // +/-1.5 for tipping
topLevelHeight = 14.7; // +/-1.5 for tipping
capLevelHeight = 28.3; // 8 comes from the height of the shipping element

// TOWER
wtop = 1000; // weight of the tower top (oz)
wbottom = 1000; // weight of the tower bottom (oz)

// DRIVETRAIN
rWh = 2; // the radius of the drivetrain wheel (in)
wDT = 500; // the weight of the drivetrain (oz)
nDrivers = 4; // the number of drivetrain motors
mechanum = true; // indicates if mechanum wheels are being used
motorToDriveGearRatio = 1; // the motor to drive gear ratio

// ARM
w0 = 0; // counter weight on arm
w1 = gramsToOunces(356); // weight of the upper arm (oz)
w2 = gramsToOunces(242); // weight of the lower arm (oz)
w3 = 4; // weight of the gripper (oz)
w4 = gramsToOunces(blockWeight); // weight of the heaviest block or ball (oz)
l0 = 3; // length of counter weight (in)
l1 = 8; // length of upper arm (in)
l2 = 8; // length of lower arm (in)
l3 = 6; // length of gripper (in)
r0 = 2; // distance from shoulder axis to counter weight center of mass (in)
r1 = l1/2; // tapers, distance from shoulder axis to upper arm center of mass (in)
r2 = l2/2; // tapers, distance from elbow axis to lower arm center of mass (in)
r3 = l3/2; // distance from wrist axis to center of gripper mass (in)
r4 = 3; // distance from wrist axis to center of block or ball (in)

// MOTOR SPECS
tMotor = 338; // torque of the motor (oz in)
sMotor = 312; // motor speed (rpm)
wMotor = rpmToRadPerSec(sMotor); // angular frequency (rad/sec)  of motor

// TORQUE SERVO SPECS
tTServo = 350; // torque of the servo (oz in)
sTServo = 60; // speed of the torque servo (rpm)
wTServo = rpmToRadPerSec(sTServo); // angular frequency (rad/sec) of torque servo

// SPEED SERVO SPECS
tSServo = 150; // torque of the servo (oz in)
sSServo = 145; // speed of the torque servo (rpm)
wSServo = rpmToRadPerSec(sSServo); // angular frequency (rad/sec) of torque servo

// FREE PARAMETERS (actuator states)
z = 10; // the amount of linear extension of the tower (in)
th1 = 45; // the angle of the shoulder motor (deg) relative to its mount on the tower (pointing down is 0)
th2 = 90; // the angle of the elbow motor (deg) relative to its mount on the upper arm
th3 = -45; // the angle of the wrist motor (deg) relative to its mount on the lower arm

// DEPENDENT PARAMETERS (resulting from actuator states)
ph1 = th1; // the angle of the upper arm relative to the vertical or tower
ph2 = th1 + th2; // the angle of the lower arm relative to the vertical or tower
ph3 = th1 + th2 + th3; // the angle of the wrist relative to the vertical or tower

// OTHER PARAMETERS
lineThickness = .4; // 3D Printer line thickness used for space between moving parts

// FUNCTIONS
function requiredArmLength() = sqrt(hmin*hmin + vmin*vmin); // the arm length is l1 + l2 but can be divided any way
function shoulderTorque() = r1*w1 + (l1+r2)*w2 + (l1+l2+r3)*w3 + (l1+l2+r4)*w4 - r0*w0;
function elbowTorque() = r2*w2 + (l2+r3)*w3 + (l2+r4)*w4;
function wristTorque() = r3*w3 + r4*w4;
function linearLiftWeight() = w0 + w1 + w2 + w3 + w4 + wtop;
function totalRobotWeight() = w0 + w1 + w2 + w3 + w4 + wtop + wbottom + wDT;
function mechanumForce(f) = sin(45)*f; // mechanum wheel force vector breakdown, reduces acceleration force by a factor of .707
function gramsToOunces(grams) = grams * 0.035274;
function inchesToMillimeters(inches) = inches * 25.4;
function millimetersToInches(millimeters) = millimeters/25.4;
// the driving force per wheel is the motor torque divided by the radius of the wheel * gear ratio, reduced if mechanum wheel
function forcePerWheel() = mechanum ? sin(45) * motorToDriveGearRatio * tMotor/rWh : motorToDriveGearRatio * tMotor/rWh;
function driveForce() = forcePerWheel() * nDrivers;
function maxAccel() = g*driveForce()/totalRobotWeight(); // maximum acceleration of the robot
       
function rpmToRadPerSec(rpm) = rpm*360/60*PI/180; // (rev/min)(360 deg/rev)(min/60 sec)(Pi radians/180 deg)
function topSpeed() = mechanum ? sin(45) * wMotor * rWh / motorToDriveGearRatio : wMotor * rWh / motorToDriveGearRatio;  // theoretical top speed of the wheel or chassis

// TEST FUNCTIONS hhhhheeeeeeeeeeeyyyyyyyyyyyyyyy
function testArmMovesUnderShoulder() = l1 < hmin;
function testArmReachesOutForwardToGround() = (l1 + l2)*(l1 + l2) >= hmin*hmin + vmin*vmin;
function canLiftMaxWeightAtShoulder() = shoulderTorque() <= tTServo * 1.5; // using the timing pulley for 24:16 rear ratio
function canLiftMaxWeightAtElbow() = elbowTorque() <= tTServo;
function canLiftMaxWeightAtWrist() = wristTorque() <= tTServo;

module runTests(){
    echo(w1=w1, "oz");
    echo(w2=w2, "oz");
    echo(w3=w3, "oz");
    
    echo("RUNNING TESTS");
    if(!testArmMovesUnderShoulder()){
        echo("FAILS TEST: Arm Moves Under Shoulder");
    }
    if(!testArmReachesOutForwardToGround()){
        echo("FAILS TEST: Arm Reaches Out Forward To Ground");
    }
    shoulderTorque = shoulderTorque();
    echo(shoulderTorque=shoulderTorque, " oz-in");
    if(!canLiftMaxWeightAtShoulder()){
        echo("FAILS TEST: Can Lift Max Weight At Shoulder");
    }
    elbowTorque = elbowTorque();
    echo(elbowTorque=elbowTorque, " oz-in");
    if(!canLiftMaxWeightAtElbow()){
        echo("FAILS TEST: Can Lift Max Weight At Elbow");
    }
    wristTorque = wristTorque();
    echo(wristTorque=wristTorque, " oz-in");
    if(!canLiftMaxWeightAtWrist()){
        echo("FAILS TEST: Can Lift Max Weight At Wrist");
    }
    echo("TESTING COMPLETE");
}

module arm(){
    upperSegmentX = inchesToMillimeters(l1);
    lowerSegmentX = inchesToMillimeters(l2);
    grabberSegmentX = inchesToMillimeters(l3);
   
    upperSegmentY = 75; //(mm)
    lowerSegmentY = upperSegmentY/2; //(mm)
    grabberSegmentY = lowerSegmentY/2;
    grabberSegmentYAlt = lowerSegmentY * 1.5;
   
    upperSegmentZ = 38;
    lowerSegmentZ = 38;
    grabberSegmentZ = 38;
    
    grabberInnerRadius = inchesToMillimeters(4);
    grabberDepth = inchesToMillimeters(2);
    pistonRadius = inchesToMillimeters(.5);
    
    upperRotate = th1-90;
    lowerRotate = th2;
    grabberRotate = th3;
   
    rotate([0, upperRotate, 0]) union() {
   //     translate([-upperSegmentZ/2, 0, 0]) armSegment(upperSegmentX, upperSegmentZ, upperSegmentZ,  upperSegmentY, upperSegmentY, .4);//+x
   
        translate([-lowerSegmentX+upperSegmentZ, 0, 0]) rotate([0, lowerRotate, 0]) union() {
   //         translate([-upperSegmentZ/2, 0, 0]) armSegment(lowerSegmentX, lowerSegmentZ, upperSegmentZ, lowerSegmentY, upperSegmentY/2, .4);//-x
            translate([-lowerSegmentX+lowerSegmentZ, 0, 0]) rotate([0, grabberRotate, 0]) translate([-lowerSegmentZ/2, 0, 0]) grabber(grabberSegmentX, grabberSegmentY, grabberSegmentYAlt, grabberSegmentZ, grabberInnerRadius, grabberDepth, pistonRadius);
        }
    }
   
}

module armSegment(Length = 200, startZ = 50, endZ = 30, startY = 50, endY = 25, lineThickness = .4){
   
   
    //translate([-segmentX+endZ*1.125, 0, 0]) cube([sensorX, sensorYAlt, sensorZ], true);
   
    sensorY = 23; //(mm)
    sensorYAlt = 40.5; //(mm)
    sensorX = 17 - 3; //(mm)
    sensorZ = 15; //(mm)
    sensorScrewDistance = 35.27; //(mm)
    sensorScrewRadius = 3.5; //(mm)
   
    magnetY = 22.75; //(mm)
    magnetYAlt = 11; // (mm)
    magnetX = 9.25; //(mm)
    magnetZAlt = 4.25; //(mm)
    magnetZ = 2; //(mm)
   
   
    segmentX = Length/2;
   
    //x y z
    points1 = [
    [-segmentX+startZ, startY/2, -startZ/2],//0
    [-segmentX+startZ, -startY/2, -startZ/2],//1
    [segmentX-endZ, -endY/2, -endZ/2],//2
    [segmentX-endZ, endY/2, -endZ/2],//3
    [-segmentX+startZ, startY/2, startZ/2],//4
    [-segmentX+startZ, -startY/2, startZ/2],//5
    [segmentX-endZ, -endY/2, endZ/2],//6
    [segmentX-endZ, endY/2, endZ/2] //7
    ];
   
    points2 = [
    [(-segmentX+startZ), startY/3, -startZ],//0
    [(-segmentX+startZ), -startY/3, -startZ],//1
    [(segmentX-endZ), -endY/3, -endZ],//2
    [(segmentX-endZ), endY/3, -endZ],//3
    [(-segmentX+startZ), startY/3, startZ],//4
    [(-segmentX+startZ), -startY/3, startZ],//5
    [(segmentX-endZ), -endY/3, endZ],//6
    [(segmentX-endZ), endY/3, endZ] //7
    ];
   
    faces = [
    [0,1,2,3],//0
    [7,6,5,4],//1
    [0,3,7,4],//2
    [2,1,5,6],//3
    [4,5,1,0],//4
    [3,2,6,7]//5
    ];
   
    points3 = [
    [endZ/2, endY/2, 0], //0
    [endZ/2, -endY/2, 0], //1
    [0, -endY/2, 0], //2
    [0, endY/2, 0], //3
    [endZ/2, -endY/2, -endZ/2], //4
    [endZ/2, endY/2, -endZ/2], //5
    ];
   
    points4 = [
    [endZ/4, endY, 0], //0
    [endZ/4, -endY, 0], //1
    [0, -endY, 0], //2
    [0, endY, ], //3
    [endZ/4, -endY, -endZ/4], //4
    [endZ/4, endY, -endZ/4], //5
    ];
   
    faces2 = [
    [0,1,2,3], //0 Top
    [1,0,5,4], //1 front
    [3,2,4,5], //2 bottom
    [2,1,4], //3 left
    [0,3,5], //4 right
    ];
   
    /*
    translate([segmentX-endZ/2*3-lineThickness*2, 0, -endZ/2]) difference() {
        union() {
            polyhedron(points3, faces2);
           
        }
        translate([endZ/6, 0, -endZ/18]) polyhedron(points4, faces2);
    }
    */
   
    /*
    translate([segmentX-endZ/3*2.5, 0, 0]) difference() {
        cylinder(8, endZ/3, endZ/3, true);
        cylinder(10, endZ/3.5, endZ/3.5, true);
        translate([endZ/3, 0, 0]) cube([endZ/3*2, endZ, endZ], true);
    }
    */
  
   translate([-segmentX+endZ, 0, 0]) difference() {
       union() {
    difference() {
        union() {
            polyhedron(points1, faces);
            //translate([segmentX-endZ+endZ/4, 0, 0]) cube([endZ/2, endY, endZ], true);
            translate([-segmentX+startZ-startZ/4, 0, 0]) cube([startZ/2, startY, startZ], true);
           
            translate([-segmentX+startZ/2, 0, 0]) difference(){
                rotate([90, 0, 0]) cylinder(startY, startZ/2, startZ/2, true);
                translate([startZ/2, 0, 0]) cube([startZ, startZ*2, startZ*2], true);
            }
           
        }
       
        translate([-segmentX+startZ/2, 0, 0]) cube([startZ+lineThickness*2+2, startY/2+lineThickness*2, startZ*3], true);
        translate([endZ/5, 0, 0]) polyhedron(points2, faces);
       
        translate([-segmentX+endZ*1.125, 0, 0]) cube([sensorX, sensorY, sensorZ], true);
       
       
    }
   
    translate([segmentX-endZ/2, 0, 0]) difference(){
        union() {
                rotate([90, 0, 0]) cylinder(endY, endZ/2, endZ/2, true);
                difference() {
                    translate([-endZ/4, 0, 0]) cube([endZ/2, endY, endZ], true);
                    translate([-endZ/4, 0, 0]) cube([endZ, endY/3*2, endZ*2], true);
                }
                   
                }
               
        translate([0, 0, 0]) rotate([90, 0, 0]) cylinder(endY*3, endZ/4-lineThickness, endZ/4-lineThickness, true);
               
        }
       
       
       
    translate([-segmentX+startZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(startY, startZ/4-lineThickness, startZ/4-lineThickness, true);
    }
   
    translate([segmentX-endZ/2, 0, endZ/3]) rotate([0, 90, 0]) cylinder(endZ*1.5, 1, 1, true);
    translate([segmentX-endZ/2, 0, -endZ/3]) rotate([0, 90, 0]) cylinder(endZ*1.5, 1, 1, true);
   
   
    translate([segmentX-endZ/2, 0, -endZ/2]) cube([magnetX, magnetY, magnetZ], true);
    translate([segmentX-endZ/2-magnetX/2, -magnetYAlt/2, -endZ/2]) cube([magnetX, magnetYAlt, magnetZAlt], false);
   
    translate([segmentX-endZ/2, 0, endZ/2]) cube([magnetX, magnetY, magnetZ], true);
    translate([segmentX-endZ/2-magnetX/2, -magnetYAlt/2, endZ/2-magnetZAlt]) cube([magnetX, magnetYAlt, magnetZAlt], false);
    }
}












module grabber(segmentX, segmentY, segmentYAlt, segmentZ, grabberInnerRadius, grabberDepth, pistonRadius) {
    grabberOuterRadius = grabberInnerRadius*1.125;
    
    translate([0, 0, 0]) difference() {
        armSegment(segmentX, segmentZ, segmentZ, segmentY, segmentY);
        translate([-(-segmentZ+segmentX*2)/2-lineThickness, 0, 0]) cube([segmentX*2-segmentZ, segmentYAlt*2, segmentZ*2], true);
    }
    
    translate([-segmentZ*1.625-(segmentZ/2-1)/4-grabberDepth, 0, 0]) grabCone(segmentX, segmentY, segmentYAlt, segmentZ, grabberInnerRadius, grabberDepth, pistonRadius);
    
    translate([0, 0, 0]) piston2(segmentX, segmentY, segmentZ, grabberDepth, pistonRadius);
}




module piston2(segmentX, segmentY, segmentZ, grabberDepth, pistonRadius){

    translate([-lineThickness, 0, 0]) rotate([0, -90, 0]) cylinder(1, 40, 40, center=false);

}

module pusher(){
    flywheelRadius = 30;
    flywheelThickness = 3;
    pulleyRadius = 19;
    pulleyThickness = 10;
    axleRadius = 10;
    axleLength = 25;
    // flywheel, lower
    cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
    // pulley
    translate([0, 0, flywheelThickness]) cylinder(pulleyThickness, pulleyRadius, flywheelRadius, center=false);
    // flywheel, upper
    translate([0, 0, flywheelThickness+pulleyThickness]) cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
    // axle
    cylinder(axleLength, axleRadius, flywheelRadius, center=false);
    // endcap
    translate([0, 0, axleLength]) cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
    
    //thing
    translate([flywheelRadius-3, 0, 1]) cylinder(flywheelThickness, 2, 3, false);
}

module piston(segmentX, segmentY, segmentZ, grabberDepth, pistonRadius){
    
    wallThickness = 5;
    
    points = [
    [-segmentZ/2, -pistonRadius-.4-wallThickness, -segmentZ/2], //0
    [-segmentZ/2, -pistonRadius-.4, -segmentZ/2], //1
    [segmentZ/2-wallThickness, -pistonRadius-.4, -segmentZ/2], //2
    [segmentZ/2-wallThickness, pistonRadius+.4, -segmentZ/2], //3
    [-segmentZ/2, pistonRadius+.4, -segmentZ/2], //4
    [-segmentZ/2, pistonRadius+.4+wallThickness, -segmentZ/2], //5
    [segmentZ/2, pistonRadius+.4+wallThickness, -segmentZ/2], //6
    [segmentZ/2, -pistonRadius-.4-wallThickness, -segmentZ/2],  //7
    [-segmentZ/2, -pistonRadius-.4-wallThickness, segmentZ/2], //8
    [-segmentZ/2, -pistonRadius-.4, segmentZ/2], //9
    [segmentZ/2-wallThickness, -pistonRadius-.4, segmentZ/2], //10
    [segmentZ/2-wallThickness, pistonRadius+.4, segmentZ/2], //11
    [-segmentZ/2, pistonRadius+.4, segmentZ/2], //12
    [-segmentZ/2, pistonRadius+.4+wallThickness, segmentZ/2], //13
    [segmentZ/2, pistonRadius+.4+wallThickness, segmentZ/2], //14
    [segmentZ/2, -pistonRadius-.4-wallThickness, segmentZ/2]  //15
    ];
    
    faces = [
    [8,15,7,0], //0
    [6,5,13,14], //1
    [0,1,2,3,4,5,6,7], //2
    [8,9,10,11,12,13,14,15], //3
    [9,8,0,1], //4
    [13,12,4,5], //5
    [15,14,6,7], //6
    [11,10,2,3], //7
    [10,9,1,2], //8
    [12,11,3,4], //9
    ];
    
    translate([-segmentZ/2, 0, 0]) polyhedron(points, faces, 0);
    
    translate([0, 0, 30]) difference() {
        cube([segmentZ/8, segmentY, segmentZ], true);
        translate([segmentZ/4, 0, 0]) cube([segmentZ/2+1, segmentY/3*2, segmentZ+1], true);
    }
    
}












module grabCone(segmentX, segmentY, segmentYAlt, segmentZ, grabberInnerRadius, grabberDepth, pistonRadius) {
    grabberOuterRadius = grabberInnerRadius*1.125;
    
    difference() {
        translate([0, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth, grabberOuterRadius, grabberOuterRadius, true);
        translate([0, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth*2, grabberInnerRadius, grabberInnerRadius/1.125, true);
        
        for(i = [0 : 4]){
            translate([-6, 0, 0]) rotate([i*45, 0, 0]) cylinder(grabberOuterRadius*2+1, 3, 3, true);
            translate([6, 0, 0]) rotate([i*45, 0, 0]) cylinder(grabberOuterRadius*2+1, 3, 3, true);
        }
    }
    difference() {
        union() {
            difference() {
                translate([grabberDepth, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth, grabberOuterRadius, pistonRadius*1.5+2, true);
                translate([grabberDepth, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth+1, grabberInnerRadius, pistonRadius+.4, true);
            }
        
            difference() {
                translate([grabberDepth, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth, pistonRadius*1.5+2, pistonRadius*1.5+2, true);
                translate([grabberDepth, 0, 0]) rotate([0, 90, 0]) cylinder(grabberDepth+1, pistonRadius+.4+1, pistonRadius+.4+1, true);
                }
            }
            /*
            translate([grabberDepth*1.5, 0, 0]) rotate([0, 90, 0]) union() {
                translate([-12, -12, 0]) rotate([0, 0, 0]) cylinder(grabberDepth, 1.99, 1.99, true);
                translate([12, -12, 0]) rotate([0, 0, 0]) cylinder(grabberDepth, 1.99, 1.99, true);
                translate([12, 12, 0]) rotate([0, 0, 0]) cylinder(grabberDepth, 1.99, 1.99, true);
                translate([-12, 12, 0]) rotate([0, 0, 0]) cylinder(grabberDepth, 1.99, 1.99, true);
            }
            */
    }
}














