$fn = 100;

function inchesToMillimeters(inches) = inches * 25.4;

end();

module end() {
    translate([0, -segmentZ/2, 0]) rotate([0, 0, -90]) wrist(); //wrist joint
    translate([0, holderRadius+axleDistance*2.25, 0]) pistonSetup(); // piston setup
    
    difference() {
        translate([0, segmentX+slotThickness, 0]) cube([segmentY, slotThickness*2, (pistonRadius+lineThickness+2)*2], true);
        translate([0, 0, 0]) rotate([-90, 0, 0]) cylinder(100, pistonRadius+lineThickness, pistonRadius+lineThickness, false);
    }
}

module pistonSetup() {
        //translate([0, 0, -leverZ/2 - lineThickness-(axleLength-lineThickness*2)/2]) rotate([0, 0, 90]) disk();
    translate([0, holderRadius*2, 0]) piston();
    translate([0, axleDistance*1+1+holderRadius*2, 0]) rotate([90, 0, 0])pistonCylinder();
    
    
    translate([0, (holderRadius*2+1), mountRadius+(pistonRadius+lineThickness+2)]) union() {
        rotate([0, -90, -90]) hoseHolder();
        translate([-mountRadius, 0, -slotThickness-1]) cube([mountRadius*2, slotThickness, 2], false);
            difference() {
                union() {
                    translate([-mountRadius, 0, -slotThickness-2]) cube([mountRadius*2, slotThickness, 2], false);
                }
                translate([0, -1, -(mountRadius+(pistonRadius+lineThickness+2))]) rotate([-90, 0, 0]) cylinder(100, pistonRadius+lineThickness, pistonRadius+lineThickness, false);
        }
    }
}


hoseOuterRadius = inchesToMillimeters(.25)/2; // (in)
hoseInnerRadius = inchesToMillimeters(.169)/2; // (in)
hoseWallThickness = 1;
slotThickness = 5;
hosePinRadius = 1;
hoseDistance = 10;
mountRadius = hoseOuterRadius+hoseWallThickness;

lineThickness = .8;

leverX = 50;
leverY = 10; 
leverZ = 3;

axleRadius = 2;
axleLength = 5;
axleDistance = 19;

capRadius = 4;
capThickness = 1;

holderRadius = 5; 
holderThickness = axleLength-lineThickness*2;

pistonRadius = 2.5; //(mm)
pistonLength = axleDistance*4; //(mm)

pegX = 5;
pegY = 10;
pegZ = 5;

grabThickness = inchesToMillimeters(2);
grabInnerRadius = inchesToMillimeters(2);
grabOuterRadius = inchesToMillimeters(2)+2;

transitionCalc = ((leverZ/2 + lineThickness + axleLength-lineThickness*2) - (pegZ*2+lineThickness*2)/2);








segmentY = 75/4;
segmentZ = 38;
segmentX = (axleDistance*2.25+holderRadius*3+1);






module wrist() {
    faces = [
    [0,1,2,3],//0
    [7,6,5,4],//1
    [0,3,7,4],//2
    [2,1,5,6],//3
    [4,5,1,0],//4
    [3,2,6,7]//5
    ];
    
    points1 = [
    [-segmentX/2, segmentY/2, -(pistonRadius+lineThickness+2)],//0
    [-segmentX/2, -segmentY/2, -(pistonRadius+lineThickness+2)],//1
    [segmentX/2, -segmentY/2, -segmentZ/2],//2
    [segmentX/2, segmentY/2, -segmentZ/2],//3
    [-segmentX/2, segmentY/2, (pistonRadius+lineThickness+2)],//4
    [-segmentX/2, -segmentY/2, (pistonRadius+lineThickness+2)],//5
    [segmentX/2, -segmentY/2, segmentZ/2],//6
    [segmentX/2, segmentY/2, segmentZ/2] //7
    ];
    
        translate([0, 0, 0]) difference(){
            union() {
                rotate([90, 0, 0]) cylinder(segmentY, segmentZ/2, segmentZ/2, true);
                difference() {
                    union() {
                        translate([-(segmentZ/2)/2, 0, 0]) cube([segmentZ/2, segmentY, segmentZ], true);
                        translate([-(segmentZ+axleDistance*2.25+holderRadius*3+1)/2, 0, 0]) polyhedron(points1, faces);
                    }
                    
                    translate([-(segmentZ/2+axleDistance*2.25+holderRadius*3+1)/2, 0, 0]) cube([segmentZ/2+axleDistance*2.25+holderRadius*3+2, segmentY-8, segmentZ*2], true);
                }
                
            }
            translate([segmentX-segmentZ*1.5, 0, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*1.5, hoseInnerRadius, hoseInnerRadius, true);
            translate([segmentX-segmentZ*1.5, 0, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*1.5, hoseInnerRadius, hoseInnerRadius, true);
            
            translate([0, 0, 0]) rotate([90, 0, 0]) linear_extrude(height = segmentY+1, center = true, convexity = 10, 0, slices = 20, scale = 1.0, $fn = 16) circle(r=4, $fn=6);
        }
}







//translate([0, (holderRadius*2), 0]) rotate([-90, 0, 0]) cylinder(100, pistonRadius+lineThickness, pistonRadius+lineThickness, false);

module hoseHolder() {
    difference() {
        union() {
            translate([0, 0, 1]) ring(slotThickness-1, hoseOuterRadius, mountRadius, false);
            translate([mountRadius-.9, 0, slotThickness/2]) cube([hosePinRadius*2, hosePinRadius*2, slotThickness], true);
            translate([(-hoseOuterRadius-hoseWallThickness)/2, 0, slotThickness/2]) cube([mountRadius, mountRadius*2, slotThickness], true);
        }
        translate([0, 0, -1]) cylinder(slotThickness+2, hoseOuterRadius, hoseOuterRadius, false);
    }
    
    ring(1, hoseInnerRadius, mountRadius, false);
    translate([mountRadius+hosePinRadius-.9, 0, 0]) cylinder(slotThickness+10, hosePinRadius, hosePinRadius, false);
}





module theMightyRing() {
    rotate([-90, 0, 0]) ring(grabThickness, grabInnerRadius, grabOuterRadius, false);
}

module pistonCylinder() {
    ring(axleDistance*2, pistonRadius+lineThickness, pistonRadius+lineThickness+2, true);
}

module piston(){
    //piston parts
    rotate([-90, 0, 0]) cylinder(pistonLength, pistonRadius, pistonRadius, false); //piston
    translate([0, pistonLength, 0]) rotate([-90, 0, 0]) cylinder(3, inchesToMillimeters(.5), inchesToMillimeters(.5), false); //piston
    
    translate([0, 0, 0]) rotate([0, 90, 0]) union() {
        difference() {
            union() {
                translate([0, -pegY/2, 0]) cube([holderRadius*2, pegY, holderThickness], true); //PEG
                translate([0, -pegY, 0]) ring(holderThickness, axleRadius, holderRadius, true); //PEG CIRCLE
            }
            translate([0, -pegY, 0]) cylinder(holderThickness+1, axleRadius+lineThickness, axleRadius+lineThickness, true); //PEG HOLE
        }
    /*
        translate([0, -pegY, 0]) union() {
            difference() {
                union() {
                    cylinder(holderThickness*2+lineThickness*2, holderRadius, holderRadius, true); // axle connector thing
                    translate([0, -holderRadius, 0]) cube([holderRadius*2, holderRadius*2, holderThickness*2+lineThickness*2], true); //square y = holderRadius * 3
                    //translate([0, -holderRadius*2, transitionCalc/2]) cylinder(transitionCalc+pegZ*2+lineThickness*2, holderRadius, holderRadius, true); // transiton
                }
                cube([holderRadius*2+1, holderRadius*2+1, holderThickness+lineThickness*2], true); //hole
            }
            cylinder(holderThickness*2+lineThickness*2-1, axleRadius, axleRadius, true); //axle
        }
        */
    }
}

module ring(thickness = 2, innerRadius = 0, outerRadius = 1, center = true) {
    difference() {
        cylinder(thickness, outerRadius, outerRadius, center);
        translate([0, 0, -1]) cylinder(thickness+5, innerRadius, innerRadius, center);
    }
}

module disk() {
    union() {
        //lever
        //cube([leverX, leverY, leverZ], true);
        cylinder(leverZ, leverX/2, leverX/2, true);

        //axles
        translate([axleDistance, 0, axleLength/2+leverZ/2]) cylinder(axleLength, axleRadius, axleRadius, true);
        translate([-axleDistance, 0, axleLength/2+leverZ/2]) cylinder(axleLength, axleRadius, axleRadius, true);
        translate([0, 0, -axleLength/2-leverZ/2]) cylinder(axleLength, axleRadius, axleRadius, true);

        //caps
        translate([0, 0, -axleLength*2+leverZ/2-capThickness+2]) cylinder(capThickness, capRadius, capRadius,     false);
        translate([axleDistance, 0, axleLength+leverZ/2]) cylinder(capThickness, capRadius, capRadius, false);
        translate([-axleDistance, 0, axleLength+leverZ/2]) cylinder(capThickness, capRadius, capRadius, false);
        
        difference() {
            union() {
                translate([axleDistance, 0, axleLength/2+leverZ/2]) cylinder(holderThickness, holderRadius, holderRadius, true); //wheel grabber
                //translate([axleDistance+holderRadius, 0, axleLength/2+leverZ/2]) cube([holderRadius*2, holderRadius*2, holderThickness], true);
            }
            translate([axleDistance, 0, axleLength/2+leverZ/2]) cylinder(axleLength, axleRadius+lineThickness, holderThickness, true); //wheel grabber
        }
    }
}